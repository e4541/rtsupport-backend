package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/zadiv/rtsupport/graph/generated"
	"gitlab.com/zadiv/rtsupport/graph/model"
)

func (r *channelResolver) CreatedAt(ctx context.Context, obj *model.Channel) (*string, error) {
	createdAt := fmt.Sprintf("%v", obj.CreatedAt)
	return &createdAt, nil
}

func (r *messageResolver) CreatedAt(ctx context.Context, obj *model.Message) (string, error) {
	return fmt.Sprintf("%v", obj.CreatedAt), nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input model.NewUser) (*model.User, error) {
	if input.Password1 != input.Password2 {
		return nil, errors.New("password missmatch")
	}
	args := model.User{
		Username: input.Username,
		Email:    input.Email,
		Password: input.Password2,
	}
	user, err := r.UM.Create(ctx, args)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (r *mutationResolver) UpdateUser(ctx context.Context, input model.NewUser, id string) (*model.User, error) {
	if input.Password1 != input.Password2 {
		return nil, errors.New("password missmatch")
	}
	args := model.User{
		ID:       id,
		Username: input.Username,
		Email:    input.Email,
		Password: input.Password2,
	}
	user, err := r.UM.Update(ctx, args)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (r *mutationResolver) DeleteUser(ctx context.Context, filter map[string]interface{}) (bool, error) {
	if err := r.UM.Delete(ctx, filter); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) CreateChannel(ctx context.Context, input model.NewChannel) (*model.Channel, error) {
	res, err := r.CM.Create(ctx, model.Channel{
		Name:        input.Name,
		Description: *input.Description,
	})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdateChannel(ctx context.Context, input model.NewChannel, id string) (*model.Channel, error) {
	res, err := r.CM.Update(ctx, model.Channel{
		ID:          id,
		Name:        input.Name,
		Description: *input.Description,
	})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteChannel(ctx context.Context, filter map[string]interface{}) (bool, error) {
	if err := r.CM.Delete(ctx, filter); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) CreateMessage(ctx context.Context, input model.NewMessage) (*model.Message, error) {
	regExp := regexp.MustCompile("[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}")
	if !regExp.MatchString(input.AuthorID) {
		return nil, errors.New("please enter a valid user id")
	}
	if !regExp.MatchString(input.ChannelID) {
		return nil, errors.New("please enter a valid channel id")
	}

	user, err := r.UM.Get(ctx, map[string]interface{}{"id": input.AuthorID})
	if err != nil {
		return nil, errors.New("user with the given id does not exists")
	}

	channel, err := r.CM.Get(ctx, map[string]interface{}{"id": input.ChannelID})
	if err != nil {
		return nil, errors.New("channel with the given id does not exists")
	}

	res, err := r.MM.Create(ctx, model.Message{
		Body:    input.Body,
		User:    *user,
		Channel: *channel,
	})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdateMessge(ctx context.Context, input model.NewMessage, id string) (*model.Message, error) {
	regExp := regexp.MustCompile("[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}")
	if !regExp.MatchString(input.AuthorID) {
		return nil, errors.New("please enter a valid user id")
	}
	if !regExp.MatchString(input.ChannelID) {
		return nil, errors.New("please enter a valid channel id")
	}

	user, err := r.UM.Get(ctx, map[string]interface{}{"id": input.AuthorID})
	if err != nil {
		return nil, errors.New("user with the given id does not exists")
	}

	channel, err := r.CM.Get(ctx, map[string]interface{}{"id": input.ChannelID})
	if err != nil {
		return nil, errors.New("channel with the given id does not exists")
	}

	res, err := r.MM.Update(ctx, model.Message{
		ID:      id,
		Body:    input.Body,
		User:    *user,
		Channel: *channel,
	})
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteMessage(ctx context.Context, filter map[string]interface{}) (bool, error) {
	if err := r.MM.Delete(ctx, filter); err != nil {
		return false, err
	}
	return true, nil
}

func (r *queryResolver) Users(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.User, error) {
	res, err := r.UM.All(ctx, filter, limit, offset)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) User(ctx context.Context, filter map[string]interface{}) (*model.User, error) {
	res, err := r.UM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Messages(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Message, error) {
	res, err := r.MM.All(ctx, filter, limit, offset)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Message(ctx context.Context, filter map[string]interface{}) (*model.Message, error) {
	res, err := r.MM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Channels(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Channel, error) {
	res, err := r.CM.All(ctx, filter, limit, offset)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Channel(ctx context.Context, filter map[string]interface{}) (*model.Channel, error) {
	res, err := r.CM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *subscriptionResolver) Messages(ctx context.Context) (<-chan []*model.Message, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *userResolver) CreatedAt(ctx context.Context, obj *model.User) (*string, error) {
	createdAt := fmt.Sprintf("%s", obj.CreatedAt)
	return &createdAt, nil
}

// Channel returns generated.ChannelResolver implementation.
func (r *Resolver) Channel() generated.ChannelResolver { return &channelResolver{r} }

// Message returns generated.MessageResolver implementation.
func (r *Resolver) Message() generated.MessageResolver { return &messageResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type channelResolver struct{ *Resolver }
type messageResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
