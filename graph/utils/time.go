package utils

import "time"

const (
	layoutISO = "2006-01-02"
)

func TimeFromString(str string) (*time.Time, error) {
	t, err := time.Parse(layoutISO, str)
	if err != nil {
		return nil, err
	}
	return &t, nil
}
