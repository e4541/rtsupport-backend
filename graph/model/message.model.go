package model

import "time"

type Message struct {
	ID        string     `json:"id" rethinkdb:"id,omitempty"`
	User      User       `json:"user" rethinkdb:"user_id,reference" rethinkdb_ref:"id"`
	Channel   Channel    `json:"channel" rethinkdb:"channel_id,reference" rethinkdb_ref:"id"`
	Body      string     `json:"message" rethinkdb:"body,omitempty"`
	CreatedAt *time.Time `json:"created_at" rethinkdb:"created_at"`
}
