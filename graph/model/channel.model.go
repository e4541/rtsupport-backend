package model

import "time"

type Channel struct {
	ID          string     `json:"id" rethinkdb:"id,omitempty"`
	Name        string     `json:"name" rethinkdb:"name"`
	Description string     `json:"description,omitempty" rethinkdb:"description,omitempty"`
	CreatedAt   *time.Time `json:"created_at" rethinkdb:"created_at"`
}
