package model

import "time"

type User struct {
	ID        string     `json:"id" rethinkdb:"id,omitempty" redis:"id,omitempty"`
	Email     string     `json:"email" rethinkdb:"email,omitempty" redis:"email"`
	Username  string     `json:"username" rethinkdb:"username" redis:"username"`
	Password  string     `json:"password" rethinkdb:"password" redis:"-"`
	CreatedAt *time.Time `json:"created_at" rethinkdb:"created_at" redis:"created_at,omitempty"`
}
