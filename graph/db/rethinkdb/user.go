package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/rtsupport/graph/model"
	"golang.org/x/crypto/bcrypt"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IUser interface {
	Create(ctx context.Context, args model.User) (*model.User, error)
	Update(ctx context.Context, args model.User) (*model.User, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.User, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.User, error)
	Login(ctx context.Context, username, password string) error
}

type UserManager struct {
	Session *r.Session
	tblName string
}

func NewUserManager(session *r.Session, tblName string) *UserManager {
	return &UserManager{Session: session, tblName: tblName}
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func checkPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func (um *UserManager) Create(ctx context.Context, args model.User) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	pswd, err := hashPassword(args.Password)
	if err != nil {
		return nil, err
	}
	now := time.Now()
	user := model.User{
		Username:  args.Username,
		Password:  pswd,
		Email:     args.Email,
		CreatedAt: &now,
	}
	res, err := r.Table(um.tblName).Insert(user).RunWrite(um.Session)
	if err != nil {
		return nil, err
	}
	user.ID = res.GeneratedKeys[0]
	user.Password = "-"
	return &user, nil
}
func (um *UserManager) Update(ctx context.Context, args model.User) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	input := map[string]interface{}{
		"username": args.Username,
		"password": args.Password,
		"email":    args.Email,
	}
	_, err := r.Table(um.tblName).Get(args.ID).Update(input).RunWrite(um.Session)
	if err != nil {
		return nil, err
	}
	return &model.User{
		ID:       args.ID,
		Username: args.Username,
		Password: "-",
		Email:    "-",
	}, nil
}
func (um *UserManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 250*time.Millisecond)
	defer cancel()

	_, err := r.Table(um.tblName).Filter(filter).RunWrite(um.Session)
	if err != nil {
		return err
	}
	return nil

}
func (um *UserManager) Get(ctx context.Context, filter map[string]interface{}) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	preloads := GetPreloads(ctx)
	var user model.User
	res, err := r.Table(um.tblName).Filter(filter).Pluck(preloads).Run(um.Session)
	if err != nil {
		return nil, err
	}
	res.One(&user)
	return &user, nil
}
func (um *UserManager) All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 1000*time.Millisecond)
	defer cancel()
	preloads := GetPreloads(ctx)
	var data []*model.User
	res, err := r.Table(um.tblName).Limit(limit).Filter(filter).Pluck(preloads).Run(um.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err == r.ErrEmptyResult {
		return nil, errors.New("empty result")
	}
	if err != nil {
		return nil, err
	}

	defer res.Close()
	return data, nil
}
func (um *UserManager) Login(ctx context.Context, username, password string) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	var user model.User
	filter := map[string]interface{}{"username": username}
	res, err := r.Table(um.tblName).Filter(filter).Run(um.Session)
	if err != nil {
		return nil, errors.New("cannot login with given credentials")
	}
	res.One(&user)
	defer res.Close()
	if !checkPassword(password, user.Password) {
		return nil, errors.New("cannot login with given credentials")
	}
	return &user, nil
}
