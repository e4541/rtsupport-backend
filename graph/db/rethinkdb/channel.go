package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/rtsupport/graph/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

// TODO: adding Channel encryption support

type IChannel interface {
	Create(ctx context.Context, args model.Channel) (*model.Channel, error)
	Update(ctx context.Context, args model.Channel) (*model.Channel, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.Channel, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Channel, error)
}

type ChannelManager struct {
	Session *r.Session
	tblName string
}

func NewChannelManager(session *r.Session, tblName string) *ChannelManager {
	return &ChannelManager{Session: session, tblName: tblName}
}

func (cm *ChannelManager) Create(ctx context.Context, args model.Channel) (*model.Channel, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	now := time.Now()

	args.CreatedAt = &now
	res, err := r.Table(cm.tblName).Insert(args).RunWrite(cm.Session)
	if err != nil {
		return nil, err
	}
	args.ID = res.GeneratedKeys[0]
	return &args, nil

}
func (cm *ChannelManager) Update(ctx context.Context, args model.Channel) (*model.Channel, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	input := map[string]interface{}{
		"name":        args.Name,
		"description": args.Description,
	}

	_, err := r.Table(cm.tblName).Get(args.ID).Update(input).RunWrite(cm.Session)
	if err != nil {
		return nil, err
	}

	return &args, nil
}
func (cm *ChannelManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 250*time.Millisecond)
	defer cancel()

	_, err := r.Table(cm.tblName).Filter(filter).RunWrite(cm.Session)
	if err != nil {
		return err
	}
	return nil
}
func (cm *ChannelManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Channel, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	preloads := GetPreloads(ctx)
	var channel model.Channel
	res, err := r.Table(cm.tblName).Filter(filter).Pluck(preloads).Run(cm.Session)
	if err != nil {
		return nil, err
	}
	res.One(&channel)
	return &channel, nil
}
func (cm *ChannelManager) All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Channel, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	preloads := GetPreloads(ctx)
	var data []*model.Channel
	res, err := r.Table(cm.tblName).Limit(limit).Filter(filter).Pluck(preloads).Run(cm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err == r.ErrEmptyResult {
		return nil, errors.New("empty result")
	}
	if err != nil {
		return nil, err
	}

	defer res.Close()
	return data, nil
}
