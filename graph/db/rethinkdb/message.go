package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/rtsupport/graph/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

// TODO: adding message encryption support

type IMessage interface {
	Create(ctx context.Context, args model.Message) (*model.Message, error)
	Update(ctx context.Context, args model.Message) (*model.Message, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.Message, error)
	GetUser(ctx context.Context, id string) (*model.User, error)
	GetChannel(ctx context.Context, id string) (*model.Channel, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Message, error)
}

type MessageManager struct {
	Session *r.Session
	tblName string
}

func NewMessageManager(session *r.Session, tblName string) *MessageManager {
	return &MessageManager{Session: session, tblName: tblName}
}

func (mm *MessageManager) Create(ctx context.Context, args model.Message) (*model.Message, error) {
	_, cancel := context.WithTimeout(ctx, 1200*time.Millisecond)
	defer cancel()

	now := time.Now()
	msg := model.Message{
		User:      args.User,
		Channel:   args.Channel,
		Body:      args.Body,
		CreatedAt: &now,
	}
	res, err := r.Table(mm.tblName).Insert(msg).RunWrite(mm.Session)
	if err != nil {
		return nil, err
	}
	msg.ID = res.GeneratedKeys[0]
	return &msg, nil
}

func (mm *MessageManager) Update(ctx context.Context, args model.Message) (*model.Message, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	input := map[string]interface{}{
		"body":    args.Body,
		"user":    args.User,
		"channel": args.Channel,
	}
	_, err := r.Table(mm.tblName).Get(args.ID).Update(input).RunWrite(mm.Session)
	if err != nil {
		return nil, err
	}
	return &model.Message{
		ID:   args.ID,
		Body: args.Body,
	}, nil
}

func (mm *MessageManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	_, err := r.Table(mm.tblName).Filter(filter).RunWrite(mm.Session)
	if err != nil {
		return err
	}
	return nil
}

func (mm *MessageManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Message, error) {
	preloads := GetPreloads(ctx)
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()

	var msg model.Message
	res, err := r.Table(mm.tblName).Filter(filter).Merge(func(p r.Term) interface{} {
		return map[string]interface{}{
			"channel_id": r.DB("rtsupport").Table("channels").Get(p.Field("channel_id")),
			"user_id":    r.DB("rtsupport").Table("users").Get(p.Field("user_id")),
		}
	}).Pluck(preloads).Run(mm.Session)
	if err != nil {
		return nil, err
	}
	res.One(&msg)
	return &msg, nil
}

func (mm *MessageManager) All(ctx context.Context, filter map[string]interface{}, limit int, offset int) ([]*model.Message, error) {
	preloads := GetPreloads(ctx)
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	var data []*model.Message
	res, err := r.Table(mm.tblName).Slice(limit, offset*limit).Filter(filter).Merge(func(p r.Term) interface{} {
		return map[string]interface{}{
			"channel_id": r.DB("rtsupport").Table("channels").Get(p.Field("channel_id")),
			"user_id":    r.DB("rtsupport").Table("users").Get(p.Field("user_id")),
		}
	}).Pluck(preloads).Run(mm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err != nil {
		if err == r.ErrEmptyResult {
			return nil, errors.New("empty set")
		}
		return nil, err
	}
	defer res.Close()
	return data, nil
}
