//go:generate go run github.com/99designs/gqlgen generate
package graph

import db "gitlab.com/zadiv/rtsupport/graph/db/rethinkdb"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	UM *db.UserManager
	MM *db.MessageManager
	CM *db.ChannelManager
}
