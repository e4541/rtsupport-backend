package main

import (
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/joho/godotenv"
	"gitlab.com/zadiv/rtsupport/graph"
	db "gitlab.com/zadiv/rtsupport/graph/db/rethinkdb"
	"gitlab.com/zadiv/rtsupport/graph/generated"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

func main() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}
	port := os.Getenv("PORT")
	var (
		once    sync.Once
		session *r.Session
	)

	once.Do(func() {
		session = db.Init()
	})

	mm := db.NewMessageManager(session, "messages")
	um := db.NewUserManager(session, "users")
	cm := db.NewChannelManager(session, "channels")

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		UM: um, MM: mm, CM: cm,
	}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
